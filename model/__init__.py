from model import TransformerASR
from model import TransformerHintASR

m_dict = {
    'TransformerASR': TransformerASR.TransformerASR,
    'TransformerHintASR': TransformerHintASR.TransformerHintASR,
}
