# ref: wenet dataset.py
#from multiprocessing.dummy import Process
import torch
import random
import copy
import data.loader.factory as utils
import torch.distributed as dist 

from local.utils import read_list
from torch.utils.data import IterableDataset

# import torchaudio.compliance.kaldi as kaldi
# import torchaudio.functional as TAF
# import data.loader.kaldi_io as kaldi_io

class Processer(IterableDataset):
    def __init__(
        self,
        source,
        f,
        *args,
        **kw
    ):
        assert callable(f)
        self.source = source
        self.f = f
        self.args = args
        self.kw = kw
    
    def set_epoch(self, epoch):
        self.source.set_epoch(epoch)

    def __iter__(self):
        assert self.source is not None
        return self.f((iter(self.source)), *self.args, **self.kw)

    #def apply(self, f):
    #    return Processer(self, f, *self.args, **self.kw)

class DistributedSampler:
    def __init__(
        self,
        shuffle=True,
        partition=True
    ):
        self.epoch=-1
        self.update()
        self.shuffle = shuffle
        self.partition = partition

    def update(self):
        assert dist.is_available()
        if dist.is_initialized():
            self.rank = dist.get_rank()
            self.world_size = dist.get_world_size()
        else:
            self.rank = 0
            self.world_size = 1
        worker_info = torch.utils.data.get_worker_info()
        if worker_info is None:
            self.worker_id = 0
            self.num_workers = 1
        else:
            self.worker_id = worker_info.id 
            self.num_workers = worker_info.num_workers

        return dict(
            rank = self.rank,
            world_size = self.world_size,
            worker_id = self.worker_id,
            num_workers=self.num_workers
        )

    def set_epoch(self, epoch):
        self.epoch = epoch
    
    def sample(self, data):
        indexes = list(range(len(data)))
        if self.partition:
            if self.shuffle:
                random.Random(self.epoch).shuffle(indexes)
            indexes = indexes[self.rank::self.world_size]
        indexes = indexes[self.worker_id::self.num_workers]
        data = [data[i] for i in indexes]
        return data
    
    #NOTE: KEEP THE FOLLOWING CODE!!!!
    #def sample(self, data):
    #    data = list(range(len(data)))
    #    if self.partition:
    #        if self.shuffle:
    #            random.Random(self.epoch).shuffle(data)
    #        data = data[self.rank::self.world_size]
    #    data = data[self.worker_id::self.num_workers]
    #    return data

class DataList(IterableDataset):
    def __init__(
        self,
        lists,
        self_corruption=False,
        none_target_corruption_list=None,
        rirs_list=None,
        shuffle=True,
        partition=True,
        egs_format=False
    ):
        self.lists = lists if egs_format else [lists]
        self.sampler = DistributedSampler(shuffle, partition)
        self.self_corruption = self_corruption
        if none_target_corruption_list != None:
            self.none_target_corruption_list = none_target_corruption_list if egs_format else [none_target_corruption_list]
            self.num_none_target_corruption = len(self.none_target_corruption_list)
            self.none_target_corruption = True
        else:
            self.none_target_corruption = False
        if rirs_list != None:
            self.rirs_list = rirs_list if egs_format else [rirs_list]
            self.reverb = True
        else:
            self.reverb = False
        self.egs_format = egs_format
    
    def set_epoch(self, epoch):
        self.sampler.set_epoch(epoch)
    
    def padding(self, org_list, target_len):
        org_len = len(org_list)
        assert org_len < target_len
        num_repeat = target_len // org_len
        new_list = copy.deepcopy(org_list)
        for x in range(num_repeat):
            new_list += org_list
        return new_list

    def make_corrupt_candidate(self, lists, indexes, num_candidate=5):
        idx = random.choices(indexes, k=num_candidate)
        candidate = []
        for i in idx:
            candidate.append(lists[i])
        if num_candidate == 1:
            candidate = candidate[0]
        return candidate

    def __iter__(self):
        sampler_info = self.sampler.update()
        for i, egs in enumerate(self.lists):
            egs = torch.load(egs) if self.egs_format else egs
            egs = self.sampler.sample(egs)
            indexes = [x for x in range(len(egs))]
            random.Random(self.sampler.epoch).shuffle(indexes)
            if self.self_corruption:
                self_corrupt_indexes = copy.deepcopy(indexes)
                # self_corrupt_egs = copy.deepcopy(egs)
                self_corrupt_egs = egs
                random.Random(self.sampler.epoch).shuffle(self_corrupt_indexes)
            if self.none_target_corruption:
                #none_target_corrupt_egs = self.none_target_corruption_list[i % self.num_none_target_corruption]
                none_target_corrupt_egs = random.choice(self.none_target_corruption_list)
                none_target_corrupt_egs = torch.load(none_target_corrupt_egs) \
                    if self.egs_format else none_target_corrupt_egs
                #num_none_target = len(none_target_corrupt_egs)
                #none_target_corrupt_egs = none_target_corrupt_egs[0:num_target] \
                #    if num_target <= num_none_target else self.padding(none_target_corrupt_egs, num_target)
                none_target_corrupt_egs = self.sampler.sample(none_target_corrupt_egs)
                none_target_corrupt_indexes = [x for x in range(len(none_target_corrupt_egs))]
                random.Random(self.sampler.epoch).shuffle(none_target_corrupt_indexes)
            if self.reverb:
                rirs_egs = random.choice(self.rirs_list)
                rirs_egs = torch.load(rirs_egs) if self.egs_format else rirs_egs
                rirs_egs = self.sampler.sample(rirs_egs)
                rirs_indexes = [x for x in range(len(rirs_egs))]
                random.Random(self.sampler.epoch).shuffle(rirs_indexes)
            for j, index in enumerate(indexes):
                data = dict(src=egs[index], epoch=self.sampler.epoch)
                data.update(sampler_info)
                if self.self_corruption:
                    self_corrupt_candidate = self.make_corrupt_candidate(
                        self_corrupt_egs, self_corrupt_indexes
                    )
                    data.update(self_corruption=self_corrupt_candidate)
                if self.none_target_corruption:
                    none_target_corrupt_candidate = self.make_corrupt_candidate(
                        none_target_corrupt_egs, none_target_corrupt_indexes
                    )
                    data.update(none_target_corruption=none_target_corrupt_candidate)
                if self.reverb:
                    rirs_src = self.make_corrupt_candidate(rirs_egs, rirs_indexes, num_candidate=1)
                    data.update(rirs=rirs_src)
                yield data

    #NOTE: KEEP THE FOLLOWING CODE!!!!
    #def __iter__(self):
    #    sampler_info = self.sampler.update()
    #    for i, egs in enumerate(self.lists):
    #        egs = torch.load(egs) if self.egs_format else egs
    #        indexes = self.sampler.sample(egs)
    #        num_target = len(indexes)
    #        if self.self_corruption:
    #            self_corrupt_indexes = copy.deepcopy(indexes)
    #            # self_corrupt_egs = copy.deepcopy(egs)
    #            self_corrupt_egs = copy.deepcopy(egs)
    #            random.Random(self.sampler.epoch).shuffle(self_corrupt_indexes)
    #        if self.none_target_corruption:
    #            #none_target_corrupt_egs = self.none_target_corruption_list[i % self.num_none_target_corruption]
    #            none_target_corrupt_egs = random.choice(self.none_target_corruption_list)
    #            none_target_corrupt_egs = torch.load(none_target_corrupt_egs) \
    #                if self.egs_format else none_target_corrupt_egs
    #            #num_none_target = len(none_target_corrupt_egs)
    #            #none_target_corrupt_egs = none_target_corrupt_egs[0:num_target] \
    #            #    if num_target <= num_none_target else self.padding(none_target_corrupt_egs, num_target)
    #            none_target_corrupt_indexes = self.sampler.sample(none_target_corrupt_egs)
    #        for j, index in enumerate(indexes):
    #            data = dict(src=egs[index], epoch=self.sampler.epoch)
    #            data.update(sampler_info)
    #            if self.self_corruption:
    #                self_corrupt_candidate = self.make_corrupt_candidate(
    #                    self_corrupt_egs, self_corrupt_indexes
    #                )
    #                data.update(self_corruption=self_corrupt_candidate)
    #            if self.none_target_corruption:
    #                none_target_corrupt_candidate = self.make_corrupt_candidate(
    #                    none_target_corrupt_egs, none_target_corrupt_indexes
    #                )
    #                data.update(none_target_corruption=none_target_corrupt_candidate)
    #            yield data
        #indexes = self.sampler.sample(self.lists)
        #for index in indexes:
        #    data = dict(src=self.lists[index], epoch=self.sampler.epoch)
        #    data.update(sampler_info)
        #    yield data


def Dataset(
    conf, 
    d_list,
):
    # check speech config
    sph_config = conf.get('sph_config', None)
    if not sph_config:
        raise NotImplementedError(
            "sph_config should be specific, there are no any default config for " + 
            "speech feats"
        )
    data_list_config = {'lists': d_list}
    shuffle = conf.get('shuffle', True)
    data_list_config.update({'shuffle': shuffle})
    corruption_config = {}
    if sph_config.get('self_corruption', False):
        corruption_config.update({'self_corruption': sph_config.get('self_corruption')})
        self_corruption = True 
        data_list_config.update({'self_corruption': self_corruption})

    if sph_config.get('none_target_corruption', False):
        corruption_config.update({'none_target_corruption': sph_config.get('none_target_corruption')})
        none_target_corruption_list = corruption_config['none_target_corruption']['corrupt_list']
        none_target_corruption_list = read_list(none_target_corruption_list)
        data_list_config.update({'none_target_corruption_list': none_target_corruption_list})
    if sph_config.get('rirs_list', False):
        rirs_list = sph_config['rirs_list']
        rirs_list = read_list(rirs_list)
        data_list_config.update({'rirs_list': rirs_list})
        
    egs_format = conf.get('egs_format', False)
    data_list_config.update({'egs_format': egs_format})

    # Build data list
    dataset = DataList(**data_list_config)

    # START DATA PROCESS!!!
    dataset = Processer(dataset, utils.process_raw, egs_format=egs_format)
    if len(corruption_config) > 0:
        dataset = Processer(dataset, utils.process_corruption, corruption_config)

    # prepare speech feats
    if sph_config.get("video_proc", False):
        dataset = Processer(dataset, utils.process_video)
    else:
        dataset = Processer(dataset, utils.process_speech_feats, sph_config, egs_format=egs_format)

    # process label and keyword
    # k2id = {}
    # with open("data/aishell_dt1.1_dt1.7/raw/keyword2id") as k2idf:
    #     for line in k2idf.readlines():
    #         k, id = line.strip().split(" ")
    #         k2id[k] = int(id)
    # id2k = {v:k for k,v in k2id.items()}
    dataset = Processer(dataset, utils.process_text_feats)

    # NOTE: the following code which has been commented outed is used to debug just keep them here !!!
    # phone2id = {}
    # with open("/work104/shiying/work/zeus/kws_local/data/LibSpeech/pos_phone_segment/pos.phone2id") as ff:
    #     for line in ff.readlines():
    #         line = line.strip()
    #         sym,id = line.split(" ")
    #         phone2id[sym] = int(id)
    # id2phone = {v:k for k,v in phone2id.items()}
    # lexicon = {}
    # with open("/work104/shiying/work/zeus/kws_local/data/LibSpeech/librispeech-lexicon.txt") as lf:
    #     for line in lf.readlines():
    #         w, p = line.strip().split(" ", maxsplit=1)
    #         lexicon[p] = w

    # process keyword setting
    keyword_setting = conf.get('keyword_config', None) 
    if isinstance(keyword_setting, dict):
        keyword_format = keyword_setting.get('format')
        assert keyword_format in ['sample',  'fix' , 'test']
        keyword_config = keyword_setting.get('config', {})
        if keyword_format == 'sample':
            crpt_list = copy.deepcopy(d_list)
            random.shuffle(crpt_list)
            keyword_config.update({'neg_list': crpt_list, 'neg_len': len(d_list)})
            #dataset = Processer(dataset, utils.process_sampled_keyword, **keyword_config)
            dataset = Processer(dataset, utils.process_sampled_keyword_from_label,  **keyword_config)
        elif keyword_format == 'fix':
            dataset = Processer(dataset, utils.process_fix_keyword, **keyword_config)
        elif keyword_format == 'test':
            pass
        else:
            raise NotImplementedError("keyword format only Support <sample> / <fix_with_segment>")
    
    # process list data 
    dataset = Processer(dataset, utils.process_list_data)

    # process length information
    dataset = Processer(dataset, utils.make_length)

    # make batch
    dataset = Processer(dataset, utils.make_batch, conf.get('batch_size', 256))

    # if fetch_key from config is not None, use that fetch_key
    if conf.get('fetch_key', None):
        fetch_key = conf.get('fetch_key')
        fetch_key = fetch_key.split(",")
    else:
        fetch_key = ['speech', 'label', 'keyword']

    # fetch tensor
    dataset = Processer(dataset, utils.fetch_tensor, fetch_key)
    keywords = []
    def xxx(dataset):
        for data in dataset:
            keywords.append(data['keyword'])
        keywords = list(set(keywords))
    dataset = Processer(dataset, xxx)
    return dataset, keywords

