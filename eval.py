import os
import argparse
import copy
import yaml

import torch
import torchaudio
import torch.nn as nn
import soundfile as sf

from torch.utils.data import DataLoader
from data.loader.data_loader import Dataset
from local.utils import (
    make_dict_from_file, read_list, compute_eer_skleanr, compute_topk, compute_cer, remove_duplicates_and_blank
)
from yamlinclude import YamlIncludeConstructor

SPECIAL_TOKEN = [-1, 0, 71, 72]
def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--config_file',
        required=False,
        help="config file in yaml format e.g. config/ref.yaml"
    )
    parser.add_argument(
        '--keyword2id',
        required=False,
        default=None,
        help="keyword to id files"
    )
    parser.add_argument(
        '--device',
        default='0,1',
        help=' \'0,1\', ==> means use gpu_0 and gpu_1; \'cpu\' means use cpu'
    )
    parser.add_argument(
        '--batch_size',
        default=1,
        type=int,
        help=' test batch size'
    )
    parser.add_argument(
        '--test_ckpt',
        default='avg',
        help='test num ckpt e.g. 30: means test the 30 th epoch model avg means the final model'
    )
    parser.add_argument(
        '--acc_data_list',
        required=False,
        default=None,
        help="data list used to test accuracy"
    )
    parser.add_argument(
        '--fa_data_list',
        required=False,
        default=None,
        help="data list used to test False Alarm"
    )
    parser.add_argument(
        '--losst',
        default='xent',
        help=' \'0,1\', ==> means use gpu_0 and gpu_1; \'cpu\' means use cpu'
    )
    parser.add_argument(
        '--prefix',
        default=None,
        help='Save prefix'
    )
    parser.add_argument(
        '--topk',
        default=1,
        help='topk to trigger keyword'
    )
    args = parser.parse_args()
    return args


class Evaler():
    def __init__(
        self,
        model_arch: str,
        config: dict,
        args: argparse.Namespace
    ):
        # init config info
        config = self.make_test_config(config, args)
        self.data_config = config['test_data_config']
        self.model_config = config['model_config']
        self.exp_name = config['exp_config']['exp_name']
        self.exp_dir = config['exp_config']['exp_dir']
        self.result_savepath = config['exp_config']['exp_dir'] + "/test_result_{}/".format(args.test_ckpt)
        self.topk = int(args.topk)
        if args.prefix:
            self.result_savepath = self.result_savepath + "/{}".format(args.prefix)
        if not os.path.isdir(self.result_savepath):
            os.makedirs(self.result_savepath)
        
        if args.device == 'cpu':
            device = 'cpu'
            self.master_device = 'cpu'
        else:
            device = [int(x) for x in args.device.split(",")]
            self.master_device = 'cuda:{}'.format(device[0])
        # load model 
        self.model = model_arch(**self.model_config)
        _, _, _ = self.load_ckpt()

        # make test dataset and test dataloader
        self.data_loaders = {}
        self.same = data_config['acc_data_list'].split(".")[0]
        self.snrs = data_config['acc_data_list'].split(".")[2]
        self.type = data_config['acc_data_list'].split(".")[0]
        if self.data_config['acc_data_list']:
            acc_data_list = self.data_config['acc_data_list']
            acc_test_list = read_list(acc_data_list)
            acc_test_set = Dataset(self.data_config, acc_test_list)
            acc_loader = DataLoader(
                acc_test_set,
                batch_size=None,
                num_workers=12,
                shuffle=False
            )
        else:
            acc_loader = None

        self.data_loaders.update({'acc_loader': acc_loader})

        if self.data_config['fa_data_list']:
            fa_test_list = self.data_config['fa_data_list']
            fa_test_list = read_list(fa_test_list)
            fa_test_set = Dataset(self.data_config, fa_test_list)
            fa_loader = DataLoader(fa_test_set, batch_size=None, num_workers=2, shuffle=False)
        else:
            fa_loader = None
        self.data_loaders.update({'fa_loader': fa_loader})
    
        if args.keyword2id:
            self.kw2id = make_dict_from_file(args.keyword2id)
            self.id2kw = {int(v): k for k, v in self.kw2id.items()}
    
    def load_ckpt(self):
        ckpt = self.data_config['ckpt']
        ckpt = torch.load(ckpt, map_location='cpu')
        model = ckpt['model']
        if 'cv_loss' in ckpt.keys():
            cv_loss = ckpt['cv_loss']
        else:
            cv_loss = float('inf')
        self.model.load_state_dict(model)
        return 1, 1, cv_loss

    #@torch.no_grad()
    def run(self):
        self.model.to(self.master_device)
        self.model.eval()
        eval_info = {}
        for key, loader in self.data_loaders.items():
            if loader == None:
                continue
            one_info = self.eval(loader)
            eval_info[key] = one_info
            writer = open('{}/{}.{}.{}test.acc.txt'.format(self.result_savepath, self.type, self.same, self.snrs), "w")
            tn_eer = 0
            tn_ref = 0
            for item in one_info:
                key = item['key']
                score = item['score']
                label = item['phone_label']
                length = item['sph_len']
                score = score[0:length]
                label = [l.item() for l in label]
                _, hyp = compute_topk(score, k=self.topk)
                hyp = remove_duplicates_and_blank(hyp=hyp, blank_id=0, other_special=SPECIAL_TOKEN)
                label = [l for l in label if l not in SPECIAL_TOKEN]
                n_err, n_ref = compute_cer(hyp, label)
                tn_eer += n_err
                tn_ref += n_ref
                utt_cer = (n_err / n_ref) * 100
                writer.write("="*20 + key + "="*20 + "\n")
                writer.write(" ".join([str(h) for h in hyp]) + "\n")
                writer.write(" ".join([str(l) for l in label]) + "\n")
                writer.write("Utt PER: {}\n".format(utt_cer))
                writer.write("\n\n")

                n_err, n_ref = compute_cer(hyp, label)
            writer.write("Total PER: {}".format((tn_eer/tn_ref)*100))
        torch.save(eval_info, '{}/{}.{}.{}test.result.pt'.format(self.result_savepath, self.type, self.same, self.snrs))

    #@torch.no_grad()
    def eval(self, loader):
        eval_info = []
        for batch_id, data in enumerate(loader):
            utt_keys = [k for k in data[0]]
            input_data = [d.to(self.master_device) for d in data[1:]]
            asr_hyp, score, phone_label, sph_len = self.model.evaluate(input_data)
            for i, key in enumerate(utt_keys):
                one_result = {
                    'key': key, 'score': asr_hyp[i], 'phone_label': phone_label[i], 'sph_len': sph_len[i]
                }
                eval_info.append(one_result)
        return eval_info

    def eval_acc(self, scores, targets, topK=1):
        sorted_score, top_idx = compute_topk(scores, k=topK)
        num_acc = 0
        total_num = 0
        error_idx = []
        for i, one_targets in enumerate(targets):
            one_top_idx = top_idx[i]
            if one_top_idx.item() == one_targets.item():
                num_acc += 1
            else:
                error_idx.append([i, one_top_idx])
            total_num += 1
        return num_acc, total_num, error_idx
    
    def eval_fa(self, scores, topK=1):
        sorted_score, top_idx = compute_topk(scores, k=topK)
        num_fa = 0
        total_num = 0
        for i, one_top_idx in enumerate(top_idx):
            one_top_idx = one_top_idx[i]
            if one_top_idx.item() in self.id2keyword:
                num_fa += 1
            total_num += 1
        return num_fa, total_num

    def eval_eer(self, scores, targets, exp_name=None):
        if isinstance(scores, torch.Tensor):
            scores = scores.cpu().numpy()
        if isinstance(targets, torch.Tensor):
            targets = targets.cpu().numpy()
        eer, eer_info = compute_eer_skleanr(targets, scores)
        eer_th, fpr, fnr = eer_info
        return eer, eer_info

    def make_test_config(self, config, args):
        exp_config = config['exp_config']
        exp_dir = exp_config['exp_dir']
        exp_name = exp_config['exp_name']
        test_data_config = config['test_data_config']
        n_ckpt = args.test_ckpt
        batch_size = int(args.batch_size)
        ckpt = "{}/{}_{}.pt".format(exp_dir, exp_name, n_ckpt)
        if 'self_crupt' in test_data_config:
            test_data_config.pop('self_crupt') 
        if 'wav_augment' in test_data_config['sph_config']:
            test_data_config['sph_config'].pop('wav_augment') 
        if 'mix_config' in test_data_config['sph_config']:
            test_data_config['sph_config'].pop('mix_config')
        if 'none_target_crupt' in test_data_config:
            test_data_config.pop('none_target_crupt')
        if 'self_corruption' in test_data_config['sph_config']:
            test_data_config['sph_config'].pop('self_corruption')
        if 'none_target_corruption' in test_data_config['sph_config']:
            test_data_config['sph_config'].pop('none_target_corruption')
        if 'trim_config' in test_data_config['sph_config']:
            if 'trim_dither' in test_data_config['sph_config']['trim_config']:
                test_data_config['sph_config']['trim_config'].pop('trim_dither')
            if 'dither' in test_data_config['sph_config']['trim_config']:
                test_data_config['sph_config']['trim_config'].pop('dither')
            if 'trim_type' in test_data_config['sph_config']['trim_config']:
                test_data_config['sph_config']['trim_config'].pop('trim_type')
        test_data_config['keyword_config']['format'] = 'test'
        fetch_key = "key,speech,speech_len,label,label_len,keyword,keyword_len"
        if test_data_config.get('pivot', False):
            fetch_key = "key,speech,speech_len,label,label_len,keyword_pivot,keyword_pivot_len"
            #fetch_key = "key,speech,speech_len,label,label_len,keyword,keyword_len"
        test_data_config.update({
            'ckpt': ckpt,
            'shuffle': False,
            'batch_size': batch_size,
            'fetch_key': fetch_key,
            'acc_data_list': args.acc_data_list,
            'fa_data_list': args.fa_data_list,
        })
        print (fetch_key)
        return config

if __name__ == '__main__':
    import os
    from model import m_dict
    args = get_args()
    for root in os.listdir('exp'):
        root = 'exp/'  + root
        data_config = yaml.load(open("{}/data.yaml".format(root)), Loader=yaml.FullLoader)
        exp_config = yaml.load(open("{}/exp.yaml".format(root)), Loader=yaml.FullLoader)
        exp_config.update({"exp_dir": root})
        model_config = yaml.load(open("{}/model.yaml".format(root)), Loader=yaml.FullLoader)
        config = {}

        if ('hint' in root) or ('unk' in root):
            model_arch = 'TransformerHintASR'
            data_config.update({"pivot": True})

        data_config.update({"pivot": True})
        model_arch = 'ConformerSS'
        config.update({"data_config":data_config})
        config.update({"test_data_config":data_config})
        config.update({"exp_config":exp_config})
        config.update({'model_config': model_config})
        config = yaml.load(open(config), Loader=yaml.FullLoader)
        model_arch = config['model_arch']
        model = m_dict[model_arch] 
        eval = Evaler(model, config, args)
        eval.run()
